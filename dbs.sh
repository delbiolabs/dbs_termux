#!/bin/bash
# Delbio Termux Boostrapping Script (DBS)
# by Fabio Del Bene <delbio87@gmail.com>
# ispired by Luke Smith (LARBS)
# https://github.com/LukeSmithxyz/LARBS
# License: 


### OPTIONS AND VARIABLES ###

while getopts ":r:b:p:c:h" o; do case "${o}" in
	h) printf "Optional arguments for custom use:\\n  -r: Dotfiles repository (local file or url)\\n  -p: Dependencies and programs csv (local file or url)\\n  -c: Exec specific command. use commands to see availables\\n -h: Show this message\\n" && exit ;;
	r) dotfilesrepo=${OPTARG} ;;
	b) repobranch=${OPTARG} ;;
	p) progsfile=${OPTARG} ;;
  c) exec_cmd=${OPTARG} ;;
	*) printf "Invalid option: -%s\\n" "$OPTARG" && exit ;;
esac done

[ -z "$dotfilesrepo" ] && dotfilesrepo="https://bitbucket.org/delbiolabs/dotfiles"
[ -z "$progsfile" ] && progsfile="https://bitbucket.org/delbiolabs/dbs_termux/raw/master/progs.csv"
[ -z "$repobranch" ] && repobranch="termux"

### FUNCTIONS ###

error() { clear; printf "ERROR:\\n%s\\n" "$1"; exit;}

grepseq="\"^[PNBRG]*,\""
installpkg(){ pkg install -y "$1" >/dev/null 2>&1 ;}

maininstall() { # Installs all needed programs from main repo.
	dialog --title "DBS Installation" --infobox "Installing \`$1\` ($n of $total). $1 $2" 5 70
	installpkg "$1"
	}

pipinstall() {
	dialog --title "DBS Installation" --infobox "Installing the Python package \`$1\` ($n of $total). $1 $2" 5 70
	command -v pip || installpkg python >/dev/null 2>&1 && pip install --upgrade pip >/dev/null 2>&1
	yes | pip install "$1" >/dev/null 2>&1
}

geminstall() {
  dialog --title "DBS Installation" --infobox "Installing the Ruby package \`$1\` ($n of $total). $1 $2" 5 70
  command -v gem || installpkg ruby >/dev/null 2>&1 && gem update --system >/dev/null 2>&1
  yes | gem install "$1" >/dev/null 2>&1
}

npminstall() {
	dialog --title "DBS Installation" --infobox "Installing the Nodejs package \`$1\` ($n of $total). $1 $2" 5 70
	command -v npm || installpkg nodejs >/dev/null 2>&1 && npm -g update npm >/dev/null 2>&1
	yes | npm -g install "$1" >/dev/null 2>&1
}

basherinit() {
  if [ ! -e ~/.basher ]; then
    git clone git@github.com:basherpm/basher.git $HOME/.basher >/dev/null 2>&1
  fi
  export PATH="$HOME/.basher/bin:$PATH"
  eval "$(basher init -)" >/dev/null 2>&1
}

basherinstall() {
	dialog --title "DBS Installation" --infobox "Installing the Basher package \`$1\` ($n of $total). $1 $2" 5 70
	command -v basher || basherinit >/dev/null 2>&1
	yes | basher install $1 >/dev/null 2>&1
}

gitmakeinstall() {
	progname="$(basename "$1" .git)"
  dir=$(mktemp -d)
	dialog --title "DBS Installation" --infobox "Installing \`$progname\` ($n of $total) via \`git\` and \`make\`. $(basename "$1") $2" 5 70
	git clone --depth 1 "$1" "$dir" >/dev/null 2>&1
	cd "$dir" || exit
	make PREFIX="${PREFIX}" >/dev/null 2>&1
	make install PREFIX="${PREFIX}" >/dev/null 2>&1
}

prepareprogsfile() {
	([ -f "$progsfile" ] && cat "$progsfile" | sed '/^#/d' | eval grep "$grepseq" > $PREFIX/tmp/progs.csv) || curl -Ls "$progsfile" | sed '/^#/d' | eval grep "$grepseq" > $PREFIX/tmp/progs.csv
}

installationloop() { \
  prepareprogsfile

	total=$(wc -l < $PREFIX/tmp/progs.csv)
	while IFS=, read -r tag program comment; do
		n=$((n+1))
		echo "$comment" | grep "^\".*\"$" >/dev/null 2>&1 && comment="$(echo "$comment" | sed "s/\(^\"\|\"$\)//g")"
		case "$tag" in
			"P") pipinstall "$program" "$comment" ;;
      "N") npminstall "$program" "$comment" ;;
      "B") basherinstall "$program" "$comment" ;;
      "R") geminstall "$program" "$comment" ;;
      "G") gitmakeinstall "$program" "$comment" ;;
			*) maininstall "$program" "$comment" ;;
		esac
	done < $PREFIX/tmp/progs.csv ;
}

copy_ssh_keypair() {
  dialog --title "Copy ssh keypayr" --yes-label "Copy" --no-label "No" --yesno "You need to enable ssh keypair into some service?" 100 100 && \
  cat ~/.ssh/id_rsa.pub | termux-clipboard-set
}

generate_ssh_keypair() {
  if [ ! -e ~/.ssh/id_rsa ]; then
    ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ''
  fi
  copy_ssh_keypair
  dialog --title "ssh done" --yesno "Are you done" 100 100
}

putgitrepo() {
  # Downloads a gitrepo $1 and places the files in $2 only overwriting conflicts
	dialog --infobox "Downloading and installing config files..." 4 60
	[ -z "$3" ] && branch="master" || branch="$repobranch"
	dir=$(mktemp -d)
	[ ! -d "$2" ] && mkdir -p "$2"
	git clone --recursive -b "$branch" --depth 1 "$1" "$dir" >/dev/null 2>&1
	cp -rfT "$dir" "$2"
}

installationdotfiles() {
  installpkg git || error "no connection?"
  git ls-remote "$dotfilesrepo" || error "$dotfilesrepo is not a valid git repo"

  # Install the dotfiles in the user's home directory
  putgitrepo "$dotfilesrepo" "$HOME" "$repobranch"
  rm -f "$HOME/README.md" "$HOME/LICENSE"
  # make git ignore deleted LICENSE & README.md files
  git update-index --assume-unchanged "$HOME/README.md"
  git update-index --assume-unchanged "$HOME/LICENSE"
  # make git ignore all untracked files as default
  pushd ${HOME}
    git config status.showUntrackedFiles no
  popd
}

print_resume_options() {
  installpkg dialog || error "no connection?"
  dialog --title "Resume options" --yes-label "Go" --no-label "No" --yesno "progsfile: ${progsfile}\\ndotfile: ${dotfilesrepo}\\n branch: ${repobranch}" 100 100 || error "user interrupted"
}

main () {
  pkg update && \
  pkg upgrade && \
  termux-setup-storage && \
  print_resume_options && \
  generate_ssh_keypair && \
  installationloop && \
  installationdotfiles
}

case "$exec_cmd" in
  ssh)
    generate_ssh_keypair
    ;;
  dot)
    print_resume_options
    installationdotfiles
    ;;
  commands)
    printf "available commands:
- ssh  :: generare an ssh key if not exist and copy that to clipboard
- dot  :: install a dotfiles git repo
- commands  :: show available commands
"
    ;;
  *)
    main
    ;;
esac
